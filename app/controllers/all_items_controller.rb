class AllItemsController < ApplicationController
  before_action :authenticate_user!
  before_action :get_list
  before_action :authorize_user_list!
  
  # GET /lists/1/all-items
  def index
    all_items = []
    @user.lists.each do |list|
      # TODO optimize N+1 query
      all_items.concatenate list.items
    end
    all_items_by_name = all_items.group_by { |i| i.name }

    # create all_item list (dto)
    # - name
    # - all_ids { list_id, item_id, removed }  <--- for delete
    # - exists_in_list                         <--- if exists in current list
    # (category if item in current list or most common among other lists)
    # - category
    # (if exists_in_list)
    # - id
    # - location
    # - checked
    # - removed

  end

  # POST /lists/1/all-items
  def create
    create_item_and_redirect_to all_items_url(@list)
  end

  # PATCH /lists/1/all-items/1/add
  def add
    add_item_and_redirect_back
  end

  # PATCH /lists/1/all-items/1/remove
  def remove
    remove_item_and_redirect_back
  end
end
