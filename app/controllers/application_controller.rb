class ApplicationController < ActionController::Base

  NotAuthorized = Class.new(StandardError)
  
  private

  def current_user
    if session[:user_id]
      @current_user ||= User.find(session[:user_id])
    end
  end
  helper_method :current_user

  def authenticate_user!
    if current_user
      redirect_to login_path, alert: "User not in allowlist." unless user_allowlist.include?(@current_user.id)
    else
      redirect_to login_path
    end
  end

  def authorize_user_list!
    raise NotAuthorized unless @current_user.lists.include? @list
  end

  def user_allowlist
    Set[1, 3, 4] # Until Google auth is implemented
  end
end
