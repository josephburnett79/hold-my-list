module Items
  def get_list
    @list = List.find(params[:list_id])
  end

  def set_item
    @item = @list.items.find(params[:id])
  end

  def normalize_item(item)
    item.name = item.name.strip
    item.location = item.location.strip
  end

  def deduplicate(list, item)
    items = list.items.where(name: item.name)
    if items.empty?
      return item
    else
      i = items[0]
      i.location = item.location unless item.location.empty? # Keep the new category
      return items[0]
    end
  end

  def create_item_and_redirect_to(url)

    @item = @list.items.build(item_params)
    normalize_item(@item)
    @item = deduplicate(@list, @item)
    @item.removed = nil

    respond_to do |format|
      if @item.save
        format.html { redirect_to url, notice: "#{@item.name} was added." }
        format.json { render :show, status: :created, location: @item }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @item.errors, status: :unprocessable_entity }
      end
    end
  end

  def add_item_and_redirect_back
    @item.removed = nil
    @item.save!
    redirect_back fallback_location: root_path, notice: "#{@item.name} was added."
  end

  def remove_item_and_redirect_back
    @item.removed = true
    @item.save!
    redirect_back fallback_location: root_path, notice: "#{@item.name} was removed."
  end

  def check_item_and_redirect_back
    @item.checked = true
    @item.save!
    redirect_back fallback_location: root_path
  end

  def uncheck_item_and_redirect_back
    @item.checked = nil
    @item.save!
    redirect_back fallback_location: root_path
  end
end
