class ItemsController < ApplicationController
  include Items

  before_action :authenticate_user!
  before_action :get_list
  before_action :authorize_user_list!
  before_action :set_item, only: %i[ show edit update destroy add remove check uncheck ]

  # GET /lists/1/items
  def index
    @items = @list.items
  end

  # GET /lists/1/items/1
  def show
  end

  # GET /lists/1/items/new
  def new
    @item = @list.items.build
  end

  # GET /lists/1/items/1/edit
  def edit
  end

  # POST /lists/1/items
  def create
    create_item_and_redirect_to list_url(@list)
  end

  # PATCH/PUT /lists/1/items/1
  def update
    respond_to do |format|
      if @item.update(item_params)
        normalize_item(@item)
        @item.save!
        format.html { redirect_to list_url(@list), notice: "#{@item.name} was updated." }
        format.json { render :show, status: :ok, location: @item }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @item.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH /lists/1/items/1/add
  def add
    add_item_and_redirect_back
  end

  # PATCH /lists/1/items/1/remove
  def remove
    remove_item_and_redirect_back
  end

  # PATCH /lists/1/items/1/check
  def check
    check_item_and_redirect_back
  end

  # PATCH /lists/1/items/1/uncheck
  def uncheck
    uncheck_item_and_redirect_back
  end
  
  # DELETE /lists/1/items/1
  def destroy
    @item.destroy

    respond_to do |format|
      format.html { redirect_back fallback_location: root_path, notice: "#{@item.name} was deleted." }
      format.json { head :no_content }
    end
  end

  private
    # Only allow a list of trusted parameters through.
    def item_params
      params.require(:item).permit(:name, :location, :checked, :list_id)
    end
end
