module ListsHelper
  def order
    case params[:order]
    when "name"
      "by name"
    when "location"
      "by location"
    else
      "by last used"
    end
  end

  def items_grouped_and_ordered(list, my_order=nil, filter_removed=nil)
    my_order ||= order
    items = list.items
    if filter_removed
      items = items.where(removed: nil)
    end
    case my_order
    when "by name"
      { "": items.order(:name) }
    when "by location"
      items.order(:location, :name).group_by { |i| i.location }
    when "by last used"
      { "": items.order('updated_at DESC') }
    end
  end
end
