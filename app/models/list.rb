class List < ApplicationRecord
  validates :name, :presence => true
  has_many :items
  has_many :authzs
  has_many :users, through: :authzs
end
