class User < ApplicationRecord
  validates :email, presence: true, uniqueness: true
  has_many :authzs
  has_many :lists, through: :authzs
  has_secure_password
end
