Rails.application.routes.draw do
  resources :lists do
    resources :items
  end
  resource :sessions
  resources :users

  get 'login', to: 'sessions#new', as: 'login'
  get 'logout', to: 'sessions#destroy', as: 'logout'
  get 'signup', to: 'users#new', as: 'signup'

  patch 'lists/:list_id/items/:id/add', to: 'items#add'
  patch 'lists/:list_id/items/:id/remove', to: 'items#remove'
  patch 'lists/:list_id/items/:id/check', to: 'items#check'
  patch 'lists/:list_id/items/:id/uncheck', to: 'items#uncheck'

  root 'lists#index'
end
