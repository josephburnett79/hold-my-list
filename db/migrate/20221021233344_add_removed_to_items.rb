class AddRemovedToItems < ActiveRecord::Migration[7.0]
  def change
    add_column :items, :removed, :boolean
  end
end
